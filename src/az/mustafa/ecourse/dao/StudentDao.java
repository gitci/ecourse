/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package az.mustafa.ecourse.dao;

import az.mustafa.ecourse.model.Student;
import java.util.List;

/**
 *
 * @author musta
 */
public interface StudentDao {
    List<Student> getStudentList() throws Exception;
}
