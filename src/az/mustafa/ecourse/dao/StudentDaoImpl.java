/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package az.mustafa.ecourse.dao;

import az.mustafa.ecourse.model.Student;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

/**
 *
 * @author musta
 */
public class StudentDaoImpl implements StudentDao {

    @Override
    public List<Student> getStudentList() throws Exception {
        List<Student> studentList = new ArrayList<>();
        String sql = "SELECT * FROM STUDENT WHERE ACTIVE=1";
        try ( Connection c = DbHelpher.getConnection();  PreparedStatement ps = c.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Student s = new Student();
                s.setId(rs.getLong("ID"));
                s.setName(rs.getString("NAME"));
                s.setSurname(rs.getString("Surname"));
                s.setAddress(rs.getString("ADDRESS"));
                s.setDOB(rs.getDate("DOB"));
                s.setPhone(rs.getString("Phone"));
                studentList.add(s);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentList;
    }

}
