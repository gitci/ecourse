package az.mustafa.ecourse.main;

import az.mustafa.ecourse.dao.*;
import az.mustafa.ecourse.model.Student;
import java.util.List;

/**
 *
 * @author musta
 */
public class MainClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            StudentDao studentdao = new StudentDaoImpl();
           List<Student> studentList = studentdao.getStudentList();
            for (Student student : studentList) {
                System.out.println(student);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
